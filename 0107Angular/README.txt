applicazione Web asp.net core, framework front-end: angular

Non � stato caricato node_modules di ClientApp, fare npm install

COMANDI UTILI:
npm install @angular/cli@6.0.8
ng g c new-component --module app   //da command prompt entrare in ClientApp ed eseguire il comando per creare nuovo componente

Per installare la libreria per i pulsanti ON/OFF
npm install angular2-ui-switch --save
DOCUMENTAZIONE:
http://www.wisdomofjim.com/blog/angular-2-ui-switch-a-nice-reusable-component

Per installare la libreria per gli indicatori:
npm install canvas-gauges
DOCUMENTAZIONE
https://canvas-gauges.com/documentation/user-guide/

Se le librerie non vengono importate automaticamente in app.module.ts devono essere importate manualmente.

Per ottenere la stringa di connessione ad IotHub dalla bash Azure:
az iot hub show-connection-string --hub-name {your iot hub name} --policy-name service

INSTALLATI I PACCHETTI NUGET:
- Microsoft.AspNet.WebApi.Client
- Microsoft.AspNetCore.App
- Microsoft.AspNetCore.Razor.Design
- Microsoft.NETCore.App
- Newtonsoft.Json


Sono stati creati due componenti:
- thermometer: contiene l'indicatore per la temperatura creato attraverso la libreria canvas-gauges
- progress: contiene l'indicatore per la luminosit� creato attraverso la libreria canvas-gauges

I valori vengono passati ai componenti in questo modo:
file nome_componente.html: value={{temp}}
file nome_componente.ts: @Input() temp: number;
file home.component.ts: <app-thermometer [temp]="tempInternal"></app-thermometer>
In questo modo i componenti possono essere riutilizzati.

Attualmente i dati vengono creati nel controller SensorsController e passati al frontend.
La funzione di get dalla funzione azure � commentata, in attesa che siano presenti dati reali in CosmosDb.

E' stato implementato solo il post verso IotHub per accendere un led.
