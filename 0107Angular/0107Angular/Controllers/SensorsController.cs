﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Devices;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace _0107Angular.Controllers
{
    [Route("api/[controller]")]
    public class SensorsController : Controller
    {
        //classe per passare al frontend dati fake
        public class Data
        {
            public int tempInt { get; set; }
            public int tempExt { get; set; }
            public int lightExt { get; set; }
            public int led1 { get; set; }
        }

        //connessione ad IoT Hub per invio di messaggi
        private static ServiceClient s_serviceClient;
        //sulla bash azure: az iot hub show-connection-string --hub-name {your iot hub name} --policy-name service
        private readonly static string s_connectionString = "HostName=BB-IH.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=pyDfzzWrObW6IEE9prj75MAQVpK3aZ8dZjMuYIDhjkQ=";

        //connessione per ricevere dalla funzione Azure i dati di CosmosDb
        static HttpClient client;

        // GET: api/<controller>
        [HttpGet]
        public Object Get()
        {
            //connessione alla azure function per il get di tutti i dati
            //client = new HttpClient();
            //Object values = RunAsync().GetAwaiter().GetResult();

            //creazione di dati fake
            List<Data> values = new List<Data>();
            values.Add(new Data() { tempInt = 20, tempExt = 40, lightExt = 60, led1 = 0 });

            return values;
        }

        //funzione per collegarsi tramite http alla funzione azure
        static async Task<Object> RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("https://bb-af4.azurewebsites.net/");//parte del link alla funzione azure
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                Object values = await GetValuesAsync();
                return values;

            }
            catch (Exception e)
            {
                Object values = e.Message;
                return values;
            }
        }

        //funzione per collegarsi tramite http alla funzione azure
        static async Task<Object> GetValuesAsync()
        {
            Object values = null;
            HttpResponseMessage response = await client.GetAsync("api/GetLastMessages");//seconda parte del link alla funzione azure per il get dei dati
            if (response.IsSuccessStatusCode)
            {
                values = await response.Content.ReadAsAsync<Object>();
            }
            return values;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        //post verso IotHub per comandare un attuatore
        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]dynamic var)
        {
            string val = Convert.ToString(var);
            s_serviceClient = ServiceClient.CreateFromConnectionString(s_connectionString);
            InvokeMethod(val);
            //InvokeMethod(val).GetAwaiter().GetResult();
        }

        //funzione per inviare il dato verso IoTHub. Verificare che nome del CloudDeviceMethod e nome del
        //del device siano corretti
        private static async Task InvokeMethod(string val)
        {
            //var methodInvocation = new CloudToDeviceMethod("SetTelemetryInterval") { ResponseTimeout = TimeSpan.FromSeconds(30) };
            var methodInvocation = new CloudToDeviceMethod("ExecuteAction");

            methodInvocation.SetPayloadJson(val);

            // Invoke the direct method asynchronously and get the response from the simulated device.
            var response = await s_serviceClient.InvokeDeviceMethodAsync("Raspberry", methodInvocation);

        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
