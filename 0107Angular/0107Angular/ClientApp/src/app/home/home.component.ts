import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UiSwitchModule } from "angular2-ui-switch";
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
  tempInternal: number;
  tempExternal: number;
  light: number;
  led1: number;
  step: number;
  servo: number;
  private http;
  private baseUrl;
  results: Array<any> = [];
  boardA: Array<any> = [];
  boardB: Array<any> = [];
  boardC: Array<any> = [];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
    this.getData();//all'avvio della pagina richiama la funzione getData
  }

  //get dei dati dal backend. attualmente li arrivano dei dati fake
  getData() {
    this.http.get(this.baseUrl + 'api/Sensors/').subscribe(result => {
      console.log(result);
      this.tempInternal = result[0].tempInt;
      this.tempExternal = result[0].tempExt;
      this.light = result[0].lightExt;
      this.led1 = result[0].led1;

      //prende i primi 10 risultati per schedina. quando avremo i dati reali verrà implementata
      //this.results.push.apply(this.results, result);
      //var a = 0; var b = 0; var c = 0; var i = 0;
      //while (a < 10) {
      //  if (this.results[i].boardAddress === "a") {
      //    this.boardA.push(this.results[i]);
      //    a++;
      //  } i++;
      //} console.log(this.boardA)
      //  while (b < 10) {
      //    if (this.results[i].boardAddress === "b") {
      //      this.boardB.push(this.results[i]);
      //      b++;
      //    } i++;
      //} console.log(this.boardB)
      //  while (c < 10) {
      //    if (this.results[i].boardAddress === "c") {
      //      this.boardC.push(this.results[i]);
      //      c++;
      //    } i++;
      //} console.log(this.boardC)
    }, error => console.error(error));    
  }

  //post verso IotHub per accendere il led di una schedina
  lightSwitch() {
    this.led1 = 1;
    this.led1 = JSON.parse(JSON.stringify({ "GatewayID": "ID", "BoardAddress": "Address", "led1": this.led1 }));
    console.log(this.led1);
    this.http.post(this.baseUrl + 'api/Sensors/', this.led1, httpOptions).subscribe(result => {}, error => console.error(error));
  }

  //post per attivare il motore stepper non implementato
  stepSwitch() {
    this.step = 1;
    this.step = JSON.parse(JSON.stringify({ "GatewayID": "ID", "BoardAddress": "Address", "step": this.step }));
    console.log(this.step);
  }

  //post per attivare il servo non implementato
  servoSwitch() {
    this.servo = 1;
    this.servo = JSON.parse(JSON.stringify({ "GatewayID": "ID", "BoardAddress": "Address", "servo": this.servo }));
    console.log(this.servo);
  }
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
